﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockyLib.Signals
{
    [RequireComponent(typeof(Animator))]
    public class AnimatorSignalReceiver : BaseSignalReceiver
    {
        Animator animator;

        private List<string> parameters = new List<string>();

        void Awake()
        {
            animator = GetComponent<Animator>();
            foreach (var p in animator.parameters)
                parameters.Add(p.name);
        }

        protected override void ProcessEvent(Signal.SignalEvent signalEvent)
        {
            if (!parameters.Contains(signalEvent.signal)) return; //not a valid signal
            animator.SetTrigger(signalEvent.signal);
        }
    }
}