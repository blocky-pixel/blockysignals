﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BlockyLib.Signals
{
    public static class Signal
    {
        public static bool ENABLE_LOGGING = false;
        
        public const string DEFAULT_CHANNEL = "____default_channel";
        private static readonly string RECEIVE_METHOD = "OnReceiveSignal";

        public static OnSignalHandler onSignal;
        public delegate void OnSignalHandler(SignalEvent signalEvent);

        private static Dictionary<string, Channel> channels = new Dictionary<string, Channel>();
        private static Stack<List<OnSignalHandler>> handlerBufferPool = new Stack<List<OnSignalHandler>>();

        public class SignalEvent
        {
            public SignalEvent(string signal, string channel)
            {
                this.signal = signal;
                this.channel = (channel == DEFAULT_CHANNEL ? null : channel);
            }

            public string signal { get; private set; }
            private string channel;

            public bool isForChannel(string targetChannel)
            {
                //the target channel is not the default channel, but this message is
                if (!string.IsNullOrWhiteSpace(targetChannel))
                {
                    if (channel == null) //this event is for the default channel
                        return false;
                    else
                        return targetChannel.Equals(channel, StringComparison.OrdinalIgnoreCase);
                }
                else
                { //The target channel is the default channel, so only work if the message is for the default channel
                    return channel == null;
                }
            }
        }

        private class Channel
        {
            public List<Handler> handlers = new List<Handler>();
        }

        private class Handler
        {
            public OnSignalHandler handler;
        }
        
        public static void AddDispatchHandler(OnSignalHandler handler, string channel)
        {
            if (string.IsNullOrWhiteSpace(channel)) channel = DEFAULT_CHANNEL;

            Channel channelData;

            if (!channels.TryGetValue(channel, out channelData))
            {
                channelData = new Channel();
                channels[channel] = channelData;
            }

            if (channelData.handlers.Any(h => h.handler == handler)) return; //already added
            channelData.handlers.Add(new Handler() { handler = handler });
        }

        public static void RemoveDispatchHandler(OnSignalHandler handler, string channel)
        {
            if (string.IsNullOrWhiteSpace(channel)) channel = DEFAULT_CHANNEL;

            Channel channelData;

            if (!channels.TryGetValue(channel, out channelData)) return; //unknown channel
            var handlerData = channelData.handlers.FirstOrDefault(h => h.handler == handler);
            if (handlerData == null) return; //unknown handler
            channelData.handlers.Remove(handlerData);
        }

        public static void RemoveAllHandlers(string channel)
        {
            channels.Remove(channel);
        }

        public static void Dispatch(string signal, string channel)
        {
            if (string.IsNullOrWhiteSpace(signal)) return; //don't send an empty signal
            if (string.IsNullOrWhiteSpace(channel)) channel = DEFAULT_CHANNEL;

            var signalEvent = new SignalEvent(signal, channel);

            List<OnSignalHandler> handlerBuffer;
            if (handlerBufferPool.Count > 0)
                handlerBuffer = handlerBufferPool.Pop();
            else
                handlerBuffer = new List<OnSignalHandler>();

            //Always send to the static event
            if (onSignal != null)
                handlerBuffer.Add(onSignal);

            //Add any subscribed handlers to the buffer
            Channel channelData;
            if (channels.TryGetValue(channel, out channelData))
            {
                foreach (var handlerData in channelData.handlers)
                    handlerBuffer.Add(handlerData.handler);
            }

            //Use a buffer of handlers in case any of the callbacks modify our handler list
            foreach (var handler in handlerBuffer)
            {
                try
                {
                    if (handler.Target == null) continue; //target no longer exists
                    handler.Invoke(signalEvent);
                }
                catch (System.Exception ex)
                {
                    Debug.LogException(ex);
                }
            }

            //Need to clear the buffer so we don't hold on to anything we're not meant to
            handlerBuffer.Clear();
            handlerBufferPool.Push(handlerBuffer);
        }

        public static void SendMessage(GameObject gameObject, string signal, string channel)
        {
            if (string.IsNullOrWhiteSpace(channel)) channel = DEFAULT_CHANNEL;
            var signalEvent = new Signal.SignalEvent(signal, channel);
            gameObject.SendMessage(RECEIVE_METHOD, signalEvent, SendMessageOptions.DontRequireReceiver);
        }

        public static void BroadcastMessage(GameObject gameObject, string signal, string channel)
        {
            if (string.IsNullOrWhiteSpace(channel)) channel = DEFAULT_CHANNEL;
            var signalEvent = new Signal.SignalEvent(signal, channel);
            gameObject.BroadcastMessage(RECEIVE_METHOD, signalEvent, SendMessageOptions.DontRequireReceiver);
        }

        public static void BroadcastMessageUpwards(GameObject gameObject, string signal, string channel)
        {
            if (string.IsNullOrWhiteSpace(channel)) channel = DEFAULT_CHANNEL;
            var signalEvent = new Signal.SignalEvent(signal, channel);
            gameObject.SendMessageUpwards(RECEIVE_METHOD, signalEvent, SendMessageOptions.DontRequireReceiver);
        }
    }
}