﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BlockyLib.Signals
{
    public class SignalSender : MonoBehaviour
    {

        [Tooltip("Self will only send to this game object, children will send to self and children, global will send to everyone")]
        public SendMode mode = SendMode.Self;

        [Tooltip("An optional field for filtering what receivers will receive the signal")]
        public string defaultChannel;

        [Tooltip("An optional field for defining a signal to send on a generic 'Send()' event")]
        public string defaultSignal;

        [Tooltip("If true, only the host will send this signal and dispatch it to other players. ENABLING THIS WILL INTRODUCE A 0.5s NETWORK DELAY TO THE SIGNAL.")]
        [UnityEngine.Serialization.FormerlySerializedAs("hostAuthoritive")]
        public bool hostAuthoritative;

        public enum SendMode
        {
            Global,
            Self,
            Children,
            Parents,
        }

        [ContextMenu("Send Default Signal")]
        public void SendDefault()
        {
            Send(defaultSignal);
        }

        public void Send(string signalValue)
        {
            _InternalSendSignal(signalValue);
        }

        private void _InternalSendSignal(string signalValue)
        {
            if (Signal.ENABLE_LOGGING)
                Debug.LogFormat(gameObject, "--[SIGNAL SEND] SignalSender '{0}' is sending {1} signal '{2}' on channel '{3}'", name, mode, signalValue, defaultChannel);

            var channel = defaultChannel;

            switch (mode)
            {
                case SendMode.Global:
                    Signal.Dispatch(signalValue, channel);
                    break;
                case SendMode.Children:
                    Signal.BroadcastMessage(gameObject, signalValue, channel);
                    break;
                case SendMode.Self:
                    Signal.SendMessage(gameObject, signalValue, channel);
                    break;
                case SendMode.Parents:
                    Signal.BroadcastMessageUpwards(gameObject, signalValue, channel);
                    break;
                default:
                    break;
            }
        }
    }
}
