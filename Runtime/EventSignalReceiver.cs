﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BlockyLib.Signals
{
    public class EventSignalReceiver : BaseSignalReceiver
    {
        [Header("Handlers")]
        public SignalData[] signalHandlers;

        [System.Serializable]
        public class SignalData
        {
            public string signal;
            public UnityEvent onSignal;
        }

        override protected void ProcessEvent(Signal.SignalEvent signalEvent)
        {
            foreach (var handler in signalHandlers)
            {
                if (handler.signal.Equals(signalEvent.signal, StringComparison.OrdinalIgnoreCase))
                {
                    handler.onSignal.Invoke();
                }
            }
        }
    }
}
