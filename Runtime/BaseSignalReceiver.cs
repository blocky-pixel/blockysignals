﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BlockyLib.Signals
{
    public abstract class BaseSignalReceiver : MonoBehaviour
    {
        [Header("Channel")]
        [Tooltip("Filter signals by using a channel. Leave blank to use the default channel")]
        [SerializeField] private string channel;

        [Header("Listen Modes")]
        [Tooltip("Listen for global signals - these are sent to all subscribers to a channel")]
        [SerializeField] private bool listenToGlobal = true;
        [Tooltip("Listen to GameObject message signals - these signals are sent only to game objects or their children")]
        [SerializeField] private bool listenToGameObjects = true;

        private bool isRemote;
        private int remotePlayerId;

        protected virtual void OnEnable()
        {
            SetHandler();
        }

        protected virtual void OnDisable()
        {
            RemoveHandler();
        }

        public string GetChannel()
        {
            return channel;
        }

        public void SetChannel(string newChannel)
        {
            if (channel == newChannel) return;

            //Update this listener to a new channel
            RemoveHandler();
            channel = newChannel;
            SetHandler();
        }

        private void RemoveHandler()
        {
            Signal.RemoveDispatchHandler(onReceiveDispatch, channel);
        }

        private void SetHandler()
        {
            Signal.AddDispatchHandler(onReceiveDispatch, channel);
        }

        private void OnReceiveSignal(Signal.SignalEvent signalEvent)
        {
            if (!listenToGameObjects) return;
            if (!signalEvent.isForChannel(channel))
            {
                if (Signal.ENABLE_LOGGING)
                    Debug.LogFormat(gameObject, "----[SIGNAL IGNORED] {0} '{1}' ignored signal '{2}' on channel '{3}'", GetType().Name, name, signalEvent.signal, channel);
                return;
            }

            if (Signal.ENABLE_LOGGING)
                Debug.LogFormat(gameObject, "----[SIGNAL RECEIVED] {0} '{1}' received signal '{2}' on channel '{3}'", GetType().Name, name, signalEvent.signal, channel);
            
            ProcessEvent(signalEvent);
        }

        private void onReceiveDispatch(Signal.SignalEvent signalEvent)
        {
            if (!listenToGlobal) return;
            if (!signalEvent.isForChannel(channel))
            {
                if (Signal.ENABLE_LOGGING)
                    Debug.LogFormat(gameObject, "----[SIGNAL IGNORED] {0} '{1}' ignored signal '{2}' on channel '{3}'", GetType().Name, name, signalEvent.signal, channel);
                
                return;
            }

            if (Signal.ENABLE_LOGGING)
                Debug.LogFormat(gameObject, "----[SIGNAL RECEIVED] {0} '{1}' received signal '{2}' on channel '{3}'", GetType().Name, name, signalEvent.signal, channel);

            ProcessEvent(signalEvent);
        }

        protected abstract void ProcessEvent(Signal.SignalEvent signalEvent);
    }
}